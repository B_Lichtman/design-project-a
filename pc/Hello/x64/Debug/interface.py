from tkinter import *
import subprocess

class Interface:

	def __init__(self, master):
		self.master = master
		master.title("Ukulele hero")

		self.score = 12341234

		self.high_score = IntVar()
		self.high_score.set(123)

		
		
		self.label_score = Label(master, text="The current high score is:")
		self.score_value = Label(master, textvariable=self.high_score)

		
		self.send_score = Button(master, text="Update Score", command=lambda: self.update("Score"), width = 20)
	
		self.label_image = Label(master, text="Enter image:")

		self.entry_image = Entry(master, width = 25)
		self.send_image = Button(master, text="Send image to screen", command=lambda: self.update("Image"), width = 20)
		
		self.init_game = Button(master, text="INITIALIZE GAME", command=lambda: self.update("Initialize"), width = 20)


		# LAYOUT

		self.label_score.grid(row=0, column=0, sticky=W, padx=20, pady=(30, 5))
		
		self.score_value.grid(row=1, column=0, padx=20, pady=5)
		
		self.send_score.grid(row=2, column=0, sticky=W, padx=20, pady=(5,5))

		self.label_image.grid(row=3, column=0, sticky=W, padx=20, pady=(30,5))

		self.entry_image.grid(row=4, column=0, sticky=W, padx=20, pady=(5, 5))

		self.send_image.grid(row=5, column=0, columnspan=2, padx=20, pady=(10,30))
		
		self.init_game.grid(row=6, column=0, columnspan=2, padx=20, pady=(10,30))





	def update(self, method):
		if method == "Score":
			print("Score update")
			return_val = subprocess.check_output(['Hello.exe'])
			integer_val = [int(s) for s in return_val.split() if s.isdigit()]
			self.high_score.set(return_val)
		
		elif method == "Image":
			print("Image update")
			
			#print(self.entry_image.get())
			return_val = subprocess.check_output(['Hello.exe', 'image', self.entry_image.get()])
			if return_val == b'SUCCEEDED':
				print("Image written")
			else:
				print("Failed to write image, try a different file name")
			
			self.entry_image.delete(0, END)
		
		elif method == "Initialize":
			print("Initialization sequence")
			
			#print(self.entry_image.get())
			return_val = subprocess.check_output(['Hello.exe', 'init'])
			if return_val == b'SUCCEEDED':
				print("Image written")
			else:
				print("Failed initialization")
		
		else:
			print("Not an option")
	
			
			
root = Tk()
my_gui = Interface(root)
root.mainloop()