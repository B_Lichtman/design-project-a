#include <stdio.h>
#include <windows.h>
#include "dpcdefs.h"
#include "dpcutil.h"
#include "bitmap_conv.h"

// Initialize error codes
ERC erc = 0;
ERC perc = 0;

// Initialise pointers
HANDLE phif;		// to store handle to device interface
TRID ptrid;			// to store transaction ID


// Initialize device data
int deviceId = 0;
char deviceName[50] = "";

int get_highscore(BYTE * score)
{
	int success;
	BYTE bAddr = 0x04;
	success = DpcGetReg(phif, bAddr, score, &perc, &ptrid);
	if(!success)
		return 1;
	return 0;
}

int write_notes(int block_ram, char * filename)
{
	int success = 0;

	char * song_bytes[] = { 0x00, 0x11, 0x22, 0x33, 0x11, 0x22,
							0x33, 0x11, 0x22, 0x33, 0x44,
							0x55, 0x11, 0x22, 0x33, 0x44 };

	BYTE bAddr = 0x00;
	BYTE addrData = 0x00;
	success = DpcPutReg(phif, bAddr, addrData, &perc, &ptrid);
	if(!success) return 1;
	printf("address 1\n");

	bAddr = 0x01;
	addrData = 0x01;
	success = DpcPutReg(phif, bAddr, addrData, &perc, &ptrid);
	if(!success) return 1;
	printf("address 2\n");

	bAddr = 0x02;
	addrData = 0x08;
	success = DpcPutReg(phif, bAddr, addrData, &perc, &ptrid);
	if(!success) return 1;
	printf("data num\n");

	for(int x = 0; x < sizeof(song_bytes)/sizeof(char *); x++)
	{

		BYTE bData = song_bytes[x];

		// WRITE THE SONG DATA
		BYTE bAddr = 0x03;
		success = DpcPutReg(phif, bAddr, bData, &perc, &ptrid);
		if(!success) return 1;
	}
	printf("song data write\n");

	return 0;
}

int write_image(int block_ram, char * filename)
{
	int success = 0;

	int transparency = 0;
	if ((BYTE) block_ram != 0x00)
		transparency = 1;

	bmp_data * byte_array = bitmap_conv(filename, transparency);
	if(!byte_array) return 1;
	printf("Byte array made\n");

	int width = byte_array->width;
	int height = byte_array->height;

	BYTE byte_w = (byte)width;
	BYTE byte_h = (byte)height;

	BYTE bAddr = 0x00;
	BYTE addrData = 0x00;
	success = DpcPutReg(phif, bAddr, addrData, &perc, &ptrid);
	if(!success) return 1;
	printf("did put address 1\n");

	bAddr = 0x01;
	addrData = 0x01;
	success = DpcPutReg(phif, bAddr, addrData, &perc, &ptrid);
	if(!success) return 1;
	printf("did put address 2\n");

	bAddr = 0x02;
	addrData = (BYTE) block_ram;
	success = DpcPutReg(phif, bAddr, addrData, &perc, &ptrid);
	if(!success) return 1;
	printf("about to send image\n");

	for(int x = 0; x < width * height / 2; x++)
	{
		BYTE bData = byte_array->pixel_data[x];

		bAddr = 0x03;
		success = DpcPutReg(phif, bAddr, bData, &perc, &ptrid);
		if (!success) return 1;
	}
	printf("sent image\n");

	return 0;
}





int dpc_init() {

	// Initialize and check DPC
	if (!DpcInit(&erc)) {
		//printf("DpcInit has returned false.\nThis means there is an issue attaching and initializing the DLL...\n");
		//printf("ERC IS: %d\n", erc);
		return 0;
	}
	else {
		//printf("DLL attached and initialized correctly.\n");
	};


	// Initialize devices
	DvmgStartConfigureDevices(NULL, &erc);
	if (erc != 0) {
		//printf("Cannot start configure devices\n");
		//printf("ERC IS: %d\n", erc);
		return 0;
	}
	else {
		//printf("Started to configure devices\n");
	}


	// Find a device to use
	deviceId = DvmgGetDefaultDev(&erc);
	if (deviceId == -1 || erc != 0) {
		//printf("Failed to get default device\n");
		//printf("ERC IS: %d\n", erc);
		return 0;
	}
	else {
		//printf("Default device ID is: %d\n", deviceId);
	}


	// Get device name from ID
	if (!DvmgGetDevName(deviceId, deviceName, &erc)) {
		//printf("Failed to get device name\n");
		//printf("ERC IS: %d\n", erc);
		return 0;
	}
	else {
		//printf("Device name is: %s\n", deviceName);
	}


	// Initialize data connection to device
	if (!DpcOpenData(&phif, deviceName, &perc, &ptrid)) {
		//printf("Failed to open data\n");
		//printf("ERC IS: %d\n", erc);
		return 0;
	}
	else {
		//printf("Data opened\n");
	}

	return 1;
}

int main(int argc, char ** argv)
{
	int err = 0;
	dpc_init();

	printf("Init failed\n");
	if(!argc) return 1;

	int mode = atoi(argv[1]);
	printf("Mode: %d\n", mode);

	if(mode == 0)
	{
		printf("Getting highscore\n");
		BYTE highscore;
		err = get_highscore(&highscore);
		if(err)
		{
			printf("High score failed\n");
			return 1;
		}
		printf("%d\n", highscore);
		return 0;
	}

	int block_ram = atoi(argv[2]);
	char * filename = argv[3];

	if(mode == 1)
	{
		printf("Writing notes\n");
		err = write_notes(block_ram, filename);
		return 0;
	}
	else if(mode == 2)
	{
		printf("Writing image\n");
		err = write_image(block_ram, filename);
		return 0;
	}
	else
		return 1;
}
