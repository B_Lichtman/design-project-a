#define _CRT_SECURE_NO_DEPRECATE
#include <stdio.h>
#include <windows.h>
#include "dpcdefs.h"
#include "dpcutil.h"
#include "bitmap_conv.h"
#define DEVICE_NAME_MAX 50
#define SONG_LENGTH 15
#define FILENAME_LIST_LEN 7


// Initialize error codes
ERC erc = 0;
ERC perc = 0;

// Initialise pointers
HANDLE phif;		// to store handle to device interface
TRID ptrid;			// to store transaction ID


// Initialize device data
int deviceId = 0;
char deviceName[50] = "";

int main(int argc, char** argv) {
	
	int highscore;



	if (dpc_init() != 1) {
		return 0;
	}
	

	if (argc == 1) {
		highscore = get_highscore();
		printf("%d",highscore);
		return 0;

		// Terminate DPC
		DpcTerm();

	} else if (argc == 2) {
		if (write_song() != 1 || write_note_images() != 1) {
			printf("FAILED");
			return 0;
		}

		// Terminate DPC
		DpcTerm();
	} else if (argc == 3) {
		////printf("Trying to upload file\n");

		// GET FILE INFO
		//const char filename[100];
		////printf("Enter a value :\n");
		//scanf("%s", filename);
		if (write_image(argv[2], 0x07) != 1) {
			printf("FAILED");
			return 0;
		}

		printf("SUCCEEDED");
		// Terminate DPC
		DpcTerm();

	} else {
		//printf("Wrong argument count\n");
		return 0;
	}



	return 0;
}


int get_highscore() {

	//PULSE A READ ADDRESS
	BYTE bAddr = 0x04; // --------------------------------> WHATS THE READ ADDRESS??
	BYTE pbData;
	if (!DpcGetReg(phif, bAddr, &pbData, &perc, &ptrid)) {
		//printf("HIGH SCORE READ unsuccessful\n");
	}
	else {
		//printf("HIGH SCORE READ successful\n");
	};

	return (int)pbData;
}




int write_image(char *filename, BYTE data_num) {

	int transparency = 0;
	if (data_num != 0x00) {
		transparency = 1;
	}

	bmp_data * byte_array = bitmap_conv(filename, transparency);


	if (byte_array == NULL) {
		//printf("Failed to get file\n");
		return 0;


	} else {

		int width = byte_array->width;
		int height = byte_array->height;


		BYTE byte_w = (byte)width;
		BYTE byte_h = (byte)height;


		//PULSE A WRITE ADDRESS (High and low)
		BYTE bAddr = 0x00;
		BYTE addrData = 0x00;
		if (!DpcPutReg(phif, bAddr, addrData, &perc, &ptrid)) {
			return 0;
			//printf("ADDRESS DATA write unsuccessful\n");
		}
		else {
			//printf("ADDRESS DATA write successful\n");
		};

		bAddr = 0x01;
		addrData = 0x00;
		if (!DpcPutReg(phif, bAddr, addrData, &perc, &ptrid)) {
			return 0;
			//printf("ADDRESS DATA write unsuccessful\n");
		}
		else {
			//printf("ADDRESS DATA write successful\n");
		};



		// PULSE DATA NUMBER
		bAddr = 0x02;
		addrData = data_num;
		if (!DpcPutReg(phif, bAddr, addrData, &perc, &ptrid)) {
			return 0;
			//printf("ADDRESS DATA write unsuccessful\n");
		}
		else {
			//printf("ADDRESS DATA write successful\n");
		};


		// WRITE THE IMAGE DATA
		//printf("Starting to write full image data to device\n");
		int x = 0;
		while (x < width * height * 0.5) {

			BYTE bData = byte_array->pixel_data[x];

			// WRITE THE IMAGE DATA
			bAddr = 0x03;
			if (!DpcPutReg(phif, bAddr, bData, &perc, &ptrid)) {
				//printf("Data write unsuccessful\n");
			};
			x++;
		}

		//printf("WRITTEN\n");

	}

	return 1;
}




int write_note_images() {
	const char * const filenames[FILENAME_LIST_LEN] = { "c.bmp", "d.bmp", "e.bmp", "f.bmp",
														"g.bmp", "a.bmp", "b.bmp" };
	int x = 0;
	while (x < FILENAME_LIST_LEN) {
		BYTE c = x;
		write_image(filenames[x], x);
		x++;
	}
}


int write_song() {
	char * song_bytes[SONG_LENGTH] = { 0x11, 0x22, 0x33, 0x11, 0x22,
									   0x33, 0x11, 0x22, 0x33, 0x44,
									   0x55, 0x11, 0x22, 0x33, 0x44 };


	//PULSE A WRITE ADDRESS (High and low)
	BYTE bAddr = 0x00;
	BYTE addrData = 0x00;
	if (!DpcPutReg(phif, bAddr, addrData, &perc, &ptrid)) {
		return 0;
		//printf("ADDRESS DATA write unsuccessful\n");
	}
	else {
		//printf("ADDRESS DATA write successful\n");
	};

	bAddr = 0x01;
	addrData = 0x00;
	if (!DpcPutReg(phif, bAddr, addrData, &perc, &ptrid)) {
		return 0;
		//printf("ADDRESS DATA write unsuccessful\n");
	}
	else {
		//printf("ADDRESS DATA write successful\n");
	};



	// PULSE DATA NUMBER
	bAddr = 0x02;
	addrData = 0x08;
	if (!DpcPutReg(phif, bAddr, addrData, &perc, &ptrid)) {
		return 0;
		//printf("ADDRESS DATA write unsuccessful\n");
	}
	else {
		//printf("ADDRESS DATA write successful\n");
	};



	int x = 0;
	while (x < SONG_LENGTH) {

		BYTE bData = song_bytes[x];

		// WRITE THE SONG DATA
		BYTE bAddr = 0x03;
		if (!DpcPutReg(phif, bAddr, bData, &perc, &ptrid)) {
			//printf("Data write unsuccessful\n");
		};
		x++;
	}

	return 1;
}





int dpc_init() {

	// Initialize and check DPC
	if (!DpcInit(&erc)) {
		//printf("DpcInit has returned false.\nThis means there is an issue attaching and initializing the DLL...\n");
		//printf("ERC IS: %d\n", erc);
		return 0;
	}
	else {
		//printf("DLL attached and initialized correctly.\n");
	};


	// Initialize devices
	DvmgStartConfigureDevices(NULL, &erc);
	if (erc != 0) {
		//printf("Cannot start configure devices\n");
		//printf("ERC IS: %d\n", erc);
		return 0;
	}
	else {
		//printf("Started to configure devices\n");
	}


	// Find a device to use
	deviceId = DvmgGetDefaultDev(&erc);
	if (deviceId == -1 || erc != 0) {
		//printf("Failed to get default device\n");
		//printf("ERC IS: %d\n", erc);
		return 0;
	}
	else {
		//printf("Default device ID is: %d\n", deviceId);
	}


	// Get device name from ID
	if (!DvmgGetDevName(deviceId, deviceName, &erc)) {
		//printf("Failed to get device name\n");
		//printf("ERC IS: %d\n", erc);
		return 0;
	}
	else {
		//printf("Device name is: %s\n", deviceName);
	}


	// Initialize data connection to device
	if (!DpcOpenData(&phif, deviceName, &perc, &ptrid)) {
		//printf("Failed to open data\n");
		//printf("ERC IS: %d\n", erc);
		return 0;
	}
	else {
		//printf("Data opened\n");
	}

	return 1;
}