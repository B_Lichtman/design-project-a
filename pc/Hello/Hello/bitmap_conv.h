#ifndef BITMAP_CONV_H_   /* Include guard */
#define BITMAP_CONV_H_
#endif


#define _CRT_SECURE_NO_DEPRECATE
#include <windows.h>
#include <stdio.h>
#include "libbmp.h"

typedef struct _bmp_data
{
	unsigned char *pixel_data;
	unsigned int height;
	unsigned int width;
} bmp_data;

bmp_data * bitmap_conv(const char * filename);

