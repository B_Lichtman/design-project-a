#define _CRT_SECURE_NO_DEPRECATE
#include <windows.h>
#include <stdio.h>
#include "bitmap_conv.h"
#include "libbmp.h"



int get_w(bmp_img *img) {
	int w = img->img_header.biWidth;

	return w;
}


int get_h(bmp_img *img) {
	int h = img->img_header.biHeight;

	return h;
}

unsigned char get_red(bmp_img *img, int x, int y) {
	unsigned char r = img->img_pixels[y][x].red;

	return r;
}



unsigned char get_green(bmp_img *img, int x, int y) {
	unsigned char r = img->img_pixels[y][x].green;

	return r;
}



unsigned char get_blue(bmp_img *img, int x, int y) {
	bmp_pixel r = img->img_pixels[y][x];
	return r.blue;

}


/*
 * Check if a file exist using fopen() function
 * return 1 if the file exist otherwise return 0
 */
int file_exists(const char * filename)
{
	// Save files in path /Hello/Hello
    FILE *file;
	file = fopen(filename, "rb");
    if (file != NULL){
        fclose(file);
        return 1;
    }
	perror("Failed: ");
    return 0;
}


/** www.resize-photos.com **/
bmp_data * bitmap_conv(const char * filename, int transparency)
{
	if (!file_exists(filename)) {
		return NULL;
	}


	// Process image
	bmp_img img;
	int a = bmp_img_read (&img, filename);
	unsigned int w = get_w(&img);
	unsigned int h = get_h(&img);


	// Initialize return array 
	const int total_length = w * h / 2;
	char *img_bytes;
	//img_bytes = malloc(total_length * sizeof(char));

	bmp_data * bitmap_data = malloc (sizeof(bmp_data));
	bitmap_data->height = h;
	bitmap_data->width = w;
	bitmap_data->pixel_data = malloc((total_length+1) * sizeof(char));


	// Initialize variables
	int x, y;
	int total_count = 0;

	int byte_half = 0;
	unsigned char finalByte = 0;
	
	// Translate image data into byte array
	for (y = 0, x; y < h; y++) {
		
		for (x = 0; x < w; x++) {

			// Get pixel value 0-255
			unsigned char red = (unsigned char)img.img_pixels[y][x].red;
			unsigned char green = (unsigned char)img.img_pixels[y][x].green;
			unsigned char blue = (unsigned char)img.img_pixels[y][x].blue;


			if (byte_half == 0) {
				finalByte = 0;
				byte_half = 1;
				
				// Simplify to 1 or 0 for each color
				unsigned char newRed = ((int) red < 125) ? 0x0 : 0x40;
				unsigned char newGreen = ((int) green < 125) ? 0x0 : 0x20;
				unsigned char newBlue = ((int) blue < 125) ? 0x0 : 0x010;
				unsigned char transparency_bit = 0x00;

				// create and save total RGB byte to array
				finalByte = (newRed | newGreen | newBlue);

				if (transparency == 1 && finalByte == 0x07) {
					transparency_bit = 0x80;
					finalByte = (transparency_bit | finalByte);
				}
	
				
			} else {
				byte_half = 0;
				
				// Simplify to 1 or 0 for each color
				unsigned char newRed = ((int) red < 125) ? 0x0 : 0x04;
				unsigned char newGreen = ((int) green < 125) ? 0x0 : 0x02;
				unsigned char newBlue = ((int) blue < 125) ? 0x0 : 0x01;
				unsigned char transparency_bit = 0x00;

				// create and save total RGB byte to array
				finalByte = (newRed | newGreen | newBlue | finalByte);

				if (transparency == 1 && finalByte == 0x07) {
					transparency_bit = 0x08;
					finalByte = (transparency_bit | finalByte);
				}

				bitmap_data->pixel_data[total_count] = finalByte;
				//printf("--> %x\n", (int) finalByte);
				total_count += 1;
				
			}
		}
		
	}

	// Set final byte to null so we can parse it easily
	bitmap_data->pixel_data[total_count] = '\0';


	// Free the data used by img
	bmp_img_free (&img);

	// Return final byte array
	return bitmap_data;
}

