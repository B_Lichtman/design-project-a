from tkinter import Tk, Label, Button, Entry, IntVar, END, W, E

class Interface:

	def __init__(self, master):
		self.master = master
		master.title("Ukulele hero")

		self.total = 0
		self.entered_number = 0


		self.label_score = Label(master, text="Enter High Score:")
		self.label_image = Label(master, text="Enter image:")

		vcmd = master.register(self.validate) # we have to wrap the command
		self.entry_score = Entry(master, validate="key", validatecommand=(vcmd, '%P'))


		vcmd_image = master.register(self.validate) # we have to wrap the command
		self.entry_image = Entry(master, validate="key", validatecommand=(vcmd_image, '%P'))

		self.send_score = Button(master, text="Send", command=lambda: self.update("Send"), width = 20)
		self.send_image = Button(master, text="Send", command=lambda: self.update("Send"), width = 20)

		# LAYOUT

		self.label_score.grid(row=0, column=0, sticky=W, padx=20, pady=20)

		self.entry_score.grid(row=1, column=0, columnspan=2, sticky=E, padx=20, pady=5)

		self.send_score.grid(row=2, column=0, columnspan=2, padx=20, pady=(30,20))

		self.label_image.grid(row=3, column=0, sticky=W, padx=20, pady=5)

		self.entry_image.grid(row=4, column=0, columnspan=2, sticky=E, padx=20, pady=5)

		self.send_image.grid(row=5, column=0, columnspan=2, padx=20, pady=(30,20))


	def validate(self, new_text):
		if not new_text: # the field is being cleared
			self.entered_number = 0
			return True

		try:
			self.entered_number = int(new_text)
			return True
		except ValueError:
			return False

	def update(self, method):
		if method == "add":
			self.total += self.entered_number
		elif method == "subtract":
			self.total -= self.entered_number
		else: # reset
			self.total = 0

		self.total_label_text.set(self.total)
		self.entry.delete(0, END)

root = Tk()
my_gui = Interface(root)
root.mainloop()