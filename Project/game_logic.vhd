library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use work.my_types.all;

entity game_logic is
	generic(
		reg_cols			: integer := 32
	);
	Port (
		clk					: in std_logic;
		rst					: in std_logic;

		shift_pulse			: in std_logic;

		matrix				: in matrix;
		paddle				: in integer;

		buttons				: in std_logic_vector(num_buttons-1 downto 0);

		write_en			: out std_logic;

		write_row			: out integer;
		write_col			: out integer;
		write_data			: out note;

		sound_start 		: out std_logic;

		score				: out integer;

		score_hundreds		: out std_logic_vector(3 downto 0);
		score_tens			: out std_logic_vector(3 downto 0);
		score_ones			: out std_logic_vector(3 downto 0)
	);

end game_logic;

architecture arch of game_logic is

	signal sig_score_hundreds	: integer := 0;
	signal sig_score_tens		: integer := 0;
	signal sig_score_ones		: integer := 0;
	signal sig_score				: integer := 0;

	signal saved_buttons	: std_logic_vector(num_buttons-1 downto 0);

begin
	score <= sig_score;
	score_hundreds <= std_logic_vector(to_unsigned(sig_score_hundreds, score_hundreds'length));
	score_tens <= std_logic_vector(to_unsigned(sig_score_tens, score_tens'length));
	score_ones <= std_logic_vector(to_unsigned(sig_score_ones, score_ones'length));
	write_row <= paddle_row;
	write_col <= paddle;
	write_data <= (others=>'0');

	process(clk)
	begin
		if rising_edge(clk) then
			write_en <= '0';
			sound_start <= '0';
			saved_buttons <= buttons;
			if buttons(0) = '1' and saved_buttons(0) = '0' then
				if matrix(paddle_row)(paddle)(2 downto 0) /= "000" then
					write_en <= '1';
					sound_start <= '1';
					sig_score <= sig_score + 1;

					-- count score such that each decimal digit
					-- is separated into one std_logic_vector(3 downto 0)
					-- 0000 to 1001 which is 0 to 9
					if sig_score_ones = 9 then
						sig_score_ones <= 0;

						if sig_score_tens = 9 then
							sig_score_tens <= 0;

							if sig_score_hundreds = 9 then
								sig_score_hundreds <= 0;
							else
								sig_score_hundreds <= sig_score_hundreds + 1;
							end if;

						else
							sig_score_tens <= sig_score_tens + 1;
						end if;

					else
						sig_score_ones <= sig_score_ones + 1;
					end if;
				end if;
			end if;
		end if;
	end process;


end arch;
