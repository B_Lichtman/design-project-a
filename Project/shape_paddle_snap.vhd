library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use work.my_types.all;

entity shape_paddle_snap is
    Port (
    	-- Internal signals
    	clk			: in std_logic;
		rst			: in std_logic;

    	-- current coordination
    	x_coord		: in integer;
    	y_coord		: in integer;

    	-- Paddle current column
    	paddle_col	: in integer;

        -- output signals
        enable      : out std_logic;
    	colour_r		: out std_logic;
    	colour_g		: out std_logic;
    	colour_b		: out std_logic

	);
end shape_paddle_snap;

architecture arch of shape_paddle_snap is

	------------------------------------------------------------------------
	-- Signal Declarations
	------------------------------------------------------------------------

	------------------------------------------------------------------------
	-- Module Implementation
	------------------------------------------------------------------------
begin
    check_paddle: process(clk)
    begin
        if (rising_edge(clk)) then
            if (paddle_y_pos <= y_coord and y_coord < paddle_y_pos + paddle_height and (note_left_limit + paddle_col * x_size_pixels) <= x_coord and x_coord < (note_left_limit + (paddle_col + 1) * x_size_pixels)) then
                colour_r <= '0';
				colour_g <= '1';
                colour_b <= '1';
                enable <= '1';
            else
                colour_r <= '0';
                colour_g <= '0';
                colour_b <= '0';
                enable <= '0';
            end if;
        end if;
    end process;

end arch;

