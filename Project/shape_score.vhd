library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use work.my_types.all;

entity shape_score is
	Port (
		-- Internal signals
		clk			: in std_logic;
		rst			: in std_logic;

		-- input signals
		score_ones	: in std_logic_vector(3 downto 0);
		score_tens	: in std_logic_vector(3 downto 0);
		score_hundreds	: in std_logic_vector(3 downto 0);

		x_coord		: in integer;
    	y_coord		: in integer;

		-- output signals
		enable			: out std_logic_vector(2 downto 0);
		colour_r		: out std_logic_vector(2 downto 0);
		colour_g		: out std_logic_vector(2 downto 0);
		colour_b		: out std_logic_vector(2 downto 0)

	);
end shape_score;

architecture arch of shape_score is

	------------------------------------------------------------------------
	-- Signal Declarations
	------------------------------------------------------------------------
	------------------------------------------------------------------------
	-- Module Implementation
	------------------------------------------------------------------------
begin

	digit_ones: entity work.shape_digit
	generic map(
		digit_x_start	=> 670 + (10 + (3 * 8)) * 2,
		digit_y_start	=> 20
	)
	port map(
		clk			=> clk,
		rst			=> rst,
		value		=> score_ones,
		x_coord		=> x_coord,
    	y_coord		=> y_coord,
		enable		=> enable(2),
		colour_r	=> colour_r(2),
		colour_g	=> colour_g(2),
		colour_b	=> colour_b(2)
	);

	digit_tens: entity work.shape_digit
	generic map(
		digit_x_start	=> 670 + (10 + (3 * 8)) * 1,
		digit_y_start	=> 20
	)
	port map(
		clk			=> clk,
		rst			=> rst,
		value		=> score_tens,
		x_coord		=> x_coord,
    	y_coord		=> y_coord,
		enable		=> enable(1),
		colour_r	=> colour_r(1),
		colour_g	=> colour_g(1),
		colour_b	=> colour_b(1)
	);

	digit_hundreds: entity work.shape_digit
	generic map(
		digit_x_start	=> 670 + (10 + (3 * 8)) * 0,
		digit_y_start	=> 20
	)
	port map(
		clk			=> clk,
		rst			=> rst,
		value			=> score_hundreds,
		x_coord		=> x_coord,
    	y_coord		=> y_coord,
		enable		=> enable(0),
		colour_r		=> colour_r(0),
		colour_g		=> colour_g(0),
		colour_b		=> colour_b(0)
	);

end arch;

