library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use work.my_types.all;

entity controller is
    Port (
        clk		: in std_logic;
        rst		: in std_logic;
        sig_rd  : out std_logic;
        sig_wr  : out std_logic;
        sig_cs  : out std_logic;
        raw_input: in std_logic_vector(7 downto 0);
		value   : out std_logic_vector(7 downto 0);
		button_input	: in std_logic_vector(num_buttons-1 downto 0);
		button_debounced	: out std_logic_vector(num_buttons-1 downto 0)
    );
end controller;

architecture arch of controller is
    ------------------------------------------------------------------------
	-- Signal Declarations
	------------------------------------------------------------------------

	signal state_counter	: integer := 0;
	signal state_reg		: integer := 0;

	------------------------------------------------------------------------
	-- Module Implementation
	------------------------------------------------------------------------
begin
    io_reg: process(clk, rst) is
	begin
		if (rising_edge(clk)) then
--			old_read <= interrupt;
--			new_read <= old_read;
--			if (old_read = '1' and new_read = '0') then
--				counterintr <= counterintr + 1;
----				sig_read <= '1';
--			end if;

			button_debounced <= button_input; -- TODO Debounce this


			if(state_counter > 10000) then
				case state_reg is
					when 0 => -- rst stage
						sig_cs <= '1';
						sig_rd <= '1';
						sig_wr <= '1';
					when 1 =>
						sig_cs <= '0';
					when 2 =>
						sig_wr <= '0';
					when 3 =>
						sig_wr <= '1';
					when 4 =>
						sig_cs <= '1';
					when 5 =>
						sig_cs <= '0';
					when 6 =>
						sig_rd <= '0';
					when 7 =>
						sig_rd <= '1';
                  value <= raw_input;
					when others =>
				end case;

				if(state_reg = 8) then
					state_reg <= 0;
				else
					state_reg <= state_reg + 1;
				end if;
				state_counter <= 0;
			else
				state_counter <= state_counter + 1;
			end if;

		end if;
	end process;


end arch;
