library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.my_types.all;

--note_registers
--column for numbers of vertical line
--row for number of horizontal line
--
------column(0)***column(column-1)
--row(0)*****************
--row(1)*****************
--row(2)*****************
--row(3)*****************
--row(4)*****************
--***
--row(row-1)*************
entity note_registers is
	Port(
		clk			: in  STD_LOGIC;
		rst			: in std_logic;
		clk_en		: in  STD_LOGIC;
		row_in 		: in  row;

		write_en	: in std_logic;
		write_row	: in integer;
		write_col	: in integer;
		write_data	: in note;
		matrix_out  : out matrix
	);
end note_registers;

architecture Behavioral of note_registers is

constant init_value : note := (others => '0');

signal sig_matrix: matrix := (others => (others => init_value));

begin
	matrix_block: process(clk) is
	begin
		if (rising_edge(clk)) then
			if rst = '1' then
				sig_matrix <= (others => (others => init_value));
			end if;
			if clk_en = '1' then
				for y in 0 to rows-1 loop
					if(y = 0) then
						sig_matrix(0) <= row_in;
					else
						sig_matrix(y) <= sig_matrix(y-1);
					end if;
				end loop;

			end if;
			if write_en = '1' then
				sig_matrix(write_row)(write_col) <= write_data;
			end if;
		end if;

	end process;

	matrix_out <= sig_matrix;

end Behavioral;
