library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use work.my_types.all;

entity shape_paddle is
    Port (
    	-- Internal signals
    	clk			: in std_logic;
    	rst			: in std_logic;

        -- current coordination
    	x_coord		: in integer;
    	y_coord		: in integer;

        -- input from ADC
		-- paddle_raw      : in integer;
		paddle_raw      : in std_logic_vector(7 downto 0);

        -- output signals
		enable      	: out std_logic;
    	colour_r		: out std_logic;
    	colour_g		: out std_logic;
    	colour_b		: out std_logic

	);
end shape_paddle;

architecture arch of shape_paddle is

	------------------------------------------------------------------------
	-- Signal Declarations
	------------------------------------------------------------------------

   signal paddle_postion : integer;

	------------------------------------------------------------------------
	-- Module Implementation
	------------------------------------------------------------------------
begin
    paddle_postion <= note_left_limit + ( to_integer(unsigned(paddle_raw)) * (note_right_limit - note_left_limit - paddle_width) ) / 256;
    check_paddle: process(clk)
    begin
        if (rising_edge(clk)) then
            if (paddle_y_pos <= y_coord and y_coord < paddle_y_pos + paddle_height and  paddle_postion <= x_coord and x_coord < paddle_postion + paddle_width) then
                colour_r <= '1';
				colour_g <= '0';
                colour_b <= '0';
                enable <= '1';
            else
                colour_r <= '0';
                colour_g <= '0';
                colour_b <= '0';
                enable <= '0';
            end if;
        end if;
    end process;

end arch;

