library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use work.my_types.all;

entity timings is
	Port (
		clk				: in std_logic;
		rst				: in std_logic;

		render_pulse	: out std_logic := '0';
		render_offset	: out integer;

		shift_pulse		: out std_logic := '0';
		notes_counter	: out integer
	);
end timings;

architecture arch of timings is
	------------------------------------------------------------------------
	-- Signal Declarations
	------------------------------------------------------------------------

	signal sig_render_offset	: integer := 0;
	signal sig_notes_counter	: integer := 0;

	signal render_counter	: integer := 0;
	signal shift_counter	: integer := 0;

	------------------------------------------------------------------------
	-- Module Implementation
	------------------------------------------------------------------------
begin
	process(clk) is
	begin
		if rising_edge(clk) then
			if(rst = '1') then
				sig_notes_counter <= 0;
				--render_counter <= 0;
				--shift_counter <= 0;
				--sig_render_offset <= 0;
			end if;

			if(render_counter = render_period - 1) then
				render_counter <= 0;
				-- increment/overflow offset counter
				if sig_render_offset = y_size_pixels - 1 then
					sig_render_offset <= 0;
				else
					sig_render_offset <= sig_render_offset + 1;
				end if;
			else
				render_counter <= render_counter + 1;
			end if;

			if(shift_counter = shift_period - 1) then
				shift_counter <= 0;
				sig_notes_counter <= sig_notes_counter + 1; -- increment notes counter
			else
				shift_counter <= shift_counter + 1;
			end if;
		end if;
	end process;

	render_offset <= sig_render_offset;
	notes_counter <= sig_notes_counter;

	render_pulse <= '1' when render_counter = 0 else '0';
	shift_pulse <= '1' when shift_counter = 0 else '0';

end arch;

