library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use work.my_types.all;

entity shape_image_test is
	Port (
		-- Internal signals
		clk			: in std_logic;
		rst			: in std_logic;

		-- input image block ram siganls
		in_addr		: in integer;
    	in_data		: in std_logic_vector(7 downto 0);
		in_valid	: in std_logic;

		-- current coordination
		x_coord		: in integer;
		y_coord		: in integer;

		-- output signals
		enable      	: out std_logic;
		colour_r		: out std_logic;
		colour_g		: out std_logic;
		colour_b		: out std_logic

	);
end shape_image_test;

architecture arch of shape_image_test is

	------------------------------------------------------------------------
	-- Signal Declarations
	------------------------------------------------------------------------

	signal ram_index	: integer;

	signal pixel_lines	: integer;
	signal pixel_extra	: integer;

	signal bytes_offset	: integer;

	signal image_index	: integer;
	signal image_index_delay	: integer;
	signal image_pixel	: std_logic_vector(bits-1 downto 0);

	------------------------------------------------------------------------
	-- Module Implementation
	------------------------------------------------------------------------
begin

	ram_index <= block_ram_image_offset + image_index/2;
	image_index <= pixel_lines * image_width + pixel_extra;

	image_block_ram: entity work.block_ram port map(
    	clk			=> clk,
		sto_addr	=> in_addr,
		sto_data	=> in_data,
		sto_enable	=> in_valid,
		ld_addr		=> ram_index,
		ld_data		=> image_pixel
    );

    check_pixel: process(clk)
    begin
        if (rising_edge(clk)) then
            if (image_top_limit <= y_coord and y_coord < image_top_limit + image_height*image_y_scale and  image_left_limit <= x_coord and x_coord < image_left_limit + image_width*image_x_scale) then

                pixel_lines <= (y_coord - image_top_limit)/image_y_scale;
				pixel_extra <= (x_coord - image_left_limit)/image_x_scale;
				enable <= '1';
			else
				enable <= '0';
			end if;

			image_index_delay <= image_index;
		end if;

		if (image_index_delay mod 2 = 0) then
			colour_r <= image_pixel(6);
			colour_g <= image_pixel(5);
			colour_b <= image_pixel(4);
		else
			colour_r <= image_pixel(2);
			colour_g <= image_pixel(1);
			colour_b <= image_pixel(0);
		end if;
	end process;

end arch;

