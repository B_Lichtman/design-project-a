library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use work.my_types.all;

entity block_ram is
	Port (
		clk			: in std_logic;

		sto_addr	: in integer;
		sto_data	: in std_logic_vector(7 downto 0);
		sto_enable	: in std_logic;

		ld_addr		: in integer;
		ld_data		: out std_logic_vector(7 downto 0)
	);
end block_ram;

architecture arch of block_ram is
	signal ram			: ram_type := (others => (others => '0'));

	signal sto_addr_constr	: integer;
	signal ld_addr_constr	: integer;
begin
	sto_addr_constr <= sto_addr when 0 < sto_addr and sto_addr < 2048 else 0;
	ld_addr_constr <= ld_addr when 0 < ld_addr and ld_addr < 2048 else 0;

	process(clk)
	begin
		if rising_edge(clk) then
			ld_data <= ram(ld_addr_constr);
			if sto_enable = '1' then
				ram(sto_addr_constr) <= sto_data;
			end if;
		end if;
	end process;
end arch;
