library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use work.my_types.all;

entity sound is
	Port (
		-- From Game logic components
		matrix_in		: in matrix;
		paddle_col		: in integer;
		start_signal	: in std_logic;

		-- Internal signals
		clk			: in std_logic;
		rst			: in std_logic;

		-- External signals
		sound_wave	: out std_logic
	);
end sound;

architecture arch of sound is

	------------------------------------------------------------------------
	-- Signal Declarations
	------------------------------------------------------------------------

	signal current_note	: note;

	signal current_period		: integer := 0;
	signal note_length		: integer := 0;
	signal period_counter	: integer := 0;

	signal sig_sound_wave	: std_logic := '0';
	signal enable			: std_logic := '0';

	constant note_length_max	: integer := 50000000 / 4;
	
	type periods_arr is array(255 downto 0) of integer;
	constant periods : periods_arr := ( 
	0 => 0,
	1 => 50000000/261/2,
	2 => 50000000/293/2,
	3 => 50000000/330/2,
	4 => 50000000/349/2,
	5 => 50000000/392/2,
	6 => 50000000/440/2,
	7 => 50000000/493/2,
	others => 0);

	------------------------------------------------------------------------
	-- Module Implementation
	------------------------------------------------------------------------
begin

	-- Middle C is approximately 250 Hz
	-- clk is 50 MHz
	-- therefore 200000 clock cycles per inversion of sound wave
	-- need to do on/off 500 times to get 2 seconds of sound

	current_note <= matrix_in(paddle_row)(paddle_col);

	sound_wave <= sig_sound_wave when enable = '1' else '0';

	process(clk, rst) is
	begin
		if (rst = '1') then
			sig_sound_wave <= '0';
			current_period <= 0;
			note_length <= 0;
			period_counter <= 0;
		elsif (rising_edge(clk)) then
			if current_period /= 0 then
				if note_length >= note_length_max-1 then
					note_length <= 0;
					enable <= '0';
				else
					note_length <= note_length + 1;
				end if;

				if period_counter >= current_period-1 then
					period_counter <= 0;
					sig_sound_wave <= not sig_sound_wave;
				else
					period_counter <= period_counter + 1;
				end if;
			end if;
			
			if start_signal = '1' then
				if current_note(2 downto 0) /= "000" then
					current_period <= periods(to_integer(unsigned(current_note and "00000111")));
					period_counter <= 0;
					note_length <= 0;
					enable <= '1';
				end if;
			end if;
		end if;
	end process;

end arch;
