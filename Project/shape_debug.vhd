library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.my_types.all;

entity shape_debug is
	Port(
		clk			: in std_logic;
		reset 		: in std_logic;
		coord_x		: in integer;
		coord_y		: in integer;

		drawing		: out std_logic; -- 1 when drawing somethting else 0
		out_r		: out std_logic;
		out_g		: out std_logic;
		out_b		: out std_logic
	);
end shape_debug;

architecture Behavioral of shape_debug is
begin
	drawing <= '1';

	process(clk, reset) is
	begin
		if (reset = '1') then
			--drawing <= '0';
			out_r <= '0';
			out_g <= '0';
			out_b <= '0';
		elsif (rising_edge(clk)) then
			case (coord_x + coord_y) mod 4 is
				when 0 =>
					out_r <= '1';
					out_g <= '0';
					out_b <= '0';
				when 1 =>
					out_r <= '0';
					out_g <= '1';
					out_b <= '0';
				when 2 =>
					out_r <= '0';
					out_g <= '0';
					out_b <= '1';
				when others =>
					out_r <= '0';
					out_g <= '0';
					out_b <= '0';
			end case;
		end if;
	end process;
end Behavioral;
