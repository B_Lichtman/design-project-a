library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use work.my_types.all;

entity shape_note is
    Port (
    	-- Internal signals
    	clk			: in std_logic;
    	rst			: in std_logic;

		-- current coordination
    	x_coord		: in integer;
    	y_coord		: in integer;

		the_note		: in note;
		in_addr			: in integer;
		in_data			: in std_logic_vector(7 downto 0);
		block_ram_sel	: in std_logic_vector(6 downto 0);

		layers_active	: out std_logic;
		colour_r		: out std_logic;
		colour_g		: out std_logic;
		colour_b		: out std_logic

	);
end shape_note;

architecture arch of shape_note is

	------------------------------------------------------------------------
	-- Signal Declarations
	------------------------------------------------------------------------

	-- Input
	signal image_index_delay	: integer;

	-- Output

	signal image_index	: integer;
	signal read_index	: integer;

	type pixel_arr_type is array (6 downto 0) of std_logic_vector(7 downto 0);
	signal image_pixel_arr	: pixel_arr_type;

	signal read_pic_select	: integer;

	------------------------------------------------------------------------
	-- Module Implementation
	------------------------------------------------------------------------
begin

	read_pic_select <= to_integer(unsigned(the_note(2 downto 0))) - 1;

	loop1: for i in 0 to 6 generate
		block_ram: entity work.block_ram port map(
	    	clk			=> clk,
			sto_addr	=> in_addr,
			sto_data	=> in_data,
			sto_enable	=> block_ram_sel(i),
			ld_addr		=> read_index,
			ld_data		=> image_pixel_arr(i)
		);
	end generate;

	process(clk, rst)
	begin
		if (rising_edge(clk)) then
			if (the_note /= "00000000") then
				image_index <= y_coord * image_width + x_coord;
				read_index <= block_ram_image_offset + image_index/2;
				image_index_delay <= image_index;
				layers_active <= '1';
			else
				layers_active <= '0';
			end if;
		end if;
		if (the_note /= "00000000") then
			if (image_index_delay mod 2 = 0) then
				layers_active <= not image_pixel_arr(read_pic_select)(7);
				colour_r <= image_pixel_arr(read_pic_select)(6);
				colour_g <= image_pixel_arr(read_pic_select)(5);
				colour_b <= image_pixel_arr(read_pic_select)(4);
			else
				layers_active <= not image_pixel_arr(read_pic_select)(3);
				colour_r <= image_pixel_arr(read_pic_select)(2);
				colour_g <= image_pixel_arr(read_pic_select)(1);
				colour_b <= image_pixel_arr(read_pic_select)(0);
			end if;
		end if;
	end process;
end arch;

