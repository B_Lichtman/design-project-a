library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use work.my_types.all;

entity song_loader is
    Port (
    	clk			: in std_logic;
    	rst			: in std_logic;
		notes_counter	: in integer;
    	note_in		: in std_logic_vector(7 downto 0);
    	row_out		: out row
    );
end song_loader;

architecture arch of song_loader is

	constant init_value : note := (others => '0');
	signal cur_note	: std_logic_vector(3 downto 0);
	signal col_std_logic	: std_logic_vector(3 downto 0);
	signal col	: integer := 0;
	signal notes_counter_delay	: integer;

begin

	cur_note <= note_in(3 downto 0) when notes_counter_delay mod 2 = 1 else note_in(7 downto 4);

	process(clk, rst)
	begin
		if rst = '1' then
			--set all ram to zero
		elsif rising_edge(clk) then
			col_std_logic <= cur_note and "0011";
			col <= to_integer(unsigned(col_std_logic));
			row_out <= (others => init_value);
			row_out(col) <= "0000" & cur_note;
			notes_counter_delay <= notes_counter;
		end if;
	end process;
end arch;
