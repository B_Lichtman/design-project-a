library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use work.my_types.all;

entity shape_digit is
	Generic(
		digit_x_start		: integer;
		digit_y_start		: integer
	);
	Port (
		-- Internal signals
		clk			: in std_logic;
		rst			: in std_logic;

		-- input signals
		value		: in std_logic_vector(3 downto 0);

		x_coord		: in integer;
    	y_coord		: in integer;

		-- output signals
		enable			: out std_logic;
		colour_r		: out std_logic;
		colour_g		: out std_logic;
		colour_b		: out std_logic

	);
end shape_digit;

architecture arch of shape_digit is

	------------------------------------------------------------------------
	-- Signal Declarations
	------------------------------------------------------------------------

	signal grid_x	: integer;
	signal grid_y	: integer;

	signal index	: integer;

	constant digit_0	: std_logic_vector(dig_arr_s-1 downto 0) := "111101101101111";
	constant digit_1	: std_logic_vector(dig_arr_s-1 downto 0) := "010010010010010";
	constant digit_2	: std_logic_vector(dig_arr_s-1 downto 0) := "111001111100111";
	constant digit_3	: std_logic_vector(dig_arr_s-1 downto 0) := "111100110100111";
	constant digit_4	: std_logic_vector(dig_arr_s-1 downto 0) := "100100111101101";
	constant digit_5	: std_logic_vector(dig_arr_s-1 downto 0) := "111100111001111";
	constant digit_6	: std_logic_vector(dig_arr_s-1 downto 0) := "111101111001111";
	constant digit_7	: std_logic_vector(dig_arr_s-1 downto 0) := "100100100100111";
	constant digit_8	: std_logic_vector(dig_arr_s-1 downto 0) := "111101111101111";
	constant digit_9	: std_logic_vector(dig_arr_s-1 downto 0) := "111100111101111";

	------------------------------------------------------------------------
	-- Module Implementation
	------------------------------------------------------------------------
begin

	grid_x <= (x_coord - digit_x_start) / digit_grid_blocksize;
	grid_y <=  (y_coord - digit_y_start) / digit_grid_blocksize;

	index <= grid_x + grid_y * digit_grid_x;

	process(clk) begin
		if (rising_edge(clk)) then
			if (x_coord < digit_x_start + digit_grid_blocksize * digit_grid_x and x_coord >= digit_x_start) 
				and (y_coord < digit_y_start + digit_grid_blocksize * digit_grid_y and y_coord >= digit_y_start) then

				colour_r <= '1';
				colour_g <= '1';
				colour_b <= '1';
				
				if value = "0000" then
					enable <= digit_0(index);
				elsif value = "0001" then
					enable <= digit_1(index);
				elsif value = "0010" then
					enable <= digit_2(index);
				elsif value = "0011" then
					enable <= digit_3(index);
				elsif value = "0100" then
					enable <= digit_4(index);
				elsif value = "0101" then
					enable <= digit_5(index);
				elsif value = "0110" then
					enable <= digit_6(index);
				elsif value = "0111" then
					enable <= digit_7(index);
				elsif value = "1000" then
					enable <= digit_8(index);
				elsif value = "1001" then
					enable <= digit_9(index);
				else
					enable <= '1';
				end if;
				
				
			else
				colour_r <= '0';
				colour_g <= '0';
				colour_b <= '0';
				enable <= '0';
			end if;
		end if;
	end process;

end arch;

