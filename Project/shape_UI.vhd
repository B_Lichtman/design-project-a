library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use work.my_types.all;

entity shape_UI is
    Port (
    	-- Internal signals
    	clk			: in std_logic;
    	rst			: in std_logic;

      -- current coordination
    	x_coord		: in integer;
    	y_coord		: in integer;

		layers_active	: out std_logic;
		colour_r		: out std_logic;
		colour_g		: out std_logic;
		colour_b		: out std_logic

	);
end shape_UI;

architecture arch of shape_UI is

	------------------------------------------------------------------------
	-- Signal Declarations
	------------------------------------------------------------------------

	signal x_in_block	: integer := 0;

	------------------------------------------------------------------------
	-- Module Implementation
	------------------------------------------------------------------------
begin

	process(clk, rst)
	begin
		if (rst='1') then
			layers_active <= '0';
			colour_r <= '0';
			colour_g <= '0';
			colour_b <= '0';
		elsif (rising_edge(clk)) then
			x_in_block <= (x_coord - note_left_limit) mod x_size_pixels;
			if (note_left_limit < x_coord and x_coord <= note_right_limit) then
	            -- draw column line
	            if (x_in_block <= 0 or x_size_pixels-1 <= x_in_block) then
	                layers_active <= '1';
	                colour_r <= '1';
	                colour_g <= '1';
	                colour_b <= '1';
	            else
	                layers_active <= '0';
					    colour_r <= '0';
				       colour_g <= '0';
				       colour_b <= '0';
	            end if;
			else
				layers_active <= '0';
				colour_r <= '0';
				colour_g <= '0';
				colour_b <= '0';
			end if;

		end if;
	end process;

end arch;

