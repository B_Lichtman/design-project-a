library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

package my_types is

	-- VGA definitions
	constant x_start	: integer := 0;
	constant x_visable	: integer := 800;
	constant x_front	: integer := 800 + 40;
	constant x_pulse	: integer := 800 + 40 + 128;
	constant x_back		: integer := 800 + 40 + 128 + 88;
	constant y_start	: integer := 0;
	constant y_visable	: integer := 600;
	constant y_front	: integer := 600 + 1;
	constant y_pulse	: integer := 600 + 1 + 4;
	constant y_back		: integer := 600 + 1 + 4 + 23;

	constant layers	: integer := 8;

	constant render_period	: integer := 50000000 / 128;
	constant shift_period	: integer := render_period * 64;

	-- Note array definitions
	type ram_type is array (2047 downto 0) of std_logic_vector(7 downto 0);

	constant bits	: integer := 8;
	subtype note is std_logic_vector(bits-1 downto 0);

	constant cols	: integer := 4;
	type row is array(cols-1 downto 0) of note;

	constant rows	: integer := 16;
	type matrix is array(rows-1 downto 0) of row;

	constant note_left_limit	: integer := x_start + 144;
	constant note_right_limit	: integer := note_left_limit + 512;

	constant note_top_limit		: integer := y_start -64;
	constant note_bottom_limit	: integer := note_top_limit + 1024;

	constant x_size_pixels	: integer := (note_right_limit - note_left_limit)/cols;
	constant y_size_pixels	: integer := (note_bottom_limit - note_top_limit)/rows; -- 8 rows

	-- Paddle parameters

	constant paddle_width	: integer := 20;
	constant paddle_height	: integer := 30;
	constant paddle_y_pos	: integer := note_top_limit + y_size_pixels * 9;

	constant snap_width		: integer := x_size_pixels - 20;
	constant snap_height	: integer := y_size_pixels - 20;

	constant paddle_row		: integer := 8;

	-- Image test parameters
	constant image_left_limit	: integer := note_left_limit;
	constant image_top_limit	: integer := y_start;

	constant image_width		: integer := 64;
	constant image_height		: integer := 60;

	constant image_x_scale		: integer := 8;
	constant image_y_scale		: integer := 8;

	constant block_ram_image_offset	: integer := 3;

	-- Controller parameters
	constant num_buttons		: integer := 1;

	-- Digit Parameters
	constant digit_grid_blocksize	: integer := 8;
	constant digit_grid_x			: integer := 3;
	constant digit_grid_y			: integer := 5;
	constant dig_arr_s				: integer := digit_grid_x * digit_grid_y;

end package my_types;

package body my_types is
end my_types;
