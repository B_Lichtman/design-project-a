library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use work.my_types.all;

entity vga_io is
	Port (
		-- From Shape components
		layers_active	: in std_logic_vector(layers-1 downto 0);
		colour_r		: in std_logic_vector(layers-1 downto 0);
		colour_g		: in std_logic_vector(layers-1 downto 0);
		colour_b		: in std_logic_vector(layers-1 downto 0);

		-- Internal signals
		clk			: in std_logic;
		rst			: in std_logic;

		x_coord		: out integer;
		y_coord		: out integer;

		-- External signals
		v_sync			: out std_logic;
		h_sync			: out std_logic;
		colour_out_r	: out std_logic;
		colour_out_g	: out std_logic;
		colour_out_b	: out std_logic
	);
end vga_io;

architecture arch of vga_io is

	------------------------------------------------------------------------
	-- Signal Declarations
	------------------------------------------------------------------------

	signal x_counter	: integer := 0;
	signal y_counter	: integer := 0;
	signal video_enable	: std_logic;

	signal master_r		: std_logic;
	signal master_g		: std_logic;
	signal master_b		: std_logic;

	------------------------------------------------------------------------
	-- Module Implementation
	------------------------------------------------------------------------
begin

	increment: process(clk, rst) is
	begin
		if (rst = '1') then
			x_counter <= 0;
			y_counter <= 0;
		elsif (rising_edge(clk)) then
			if (x_counter >= x_back - 1) then
				x_counter <= 0;
				if (y_counter >= y_back - 1) then
					y_counter <= 0;
				else
					y_counter <= y_counter + 1;
				end if;
			else
				x_counter <= x_counter + 1;
			end if;
			if (x_front <= x_counter and x_counter < x_pulse) then
				h_sync <= '1';
			else
				h_sync <= '0';
			end if;
			-- Do vsync
			if (y_front <= y_counter and y_counter < y_pulse) then
				v_sync <= '1';
			else
				v_sync <= '0';
			end if;
		end if;
	end process;

	x_coord <= x_counter;
	y_coord <= y_counter;

	video_enable <= '1' when (x_counter < x_visable and y_counter < y_visable) else '0';

	colour_out_r <= master_r when video_enable = '1' else '0';
	colour_out_g <= master_g when video_enable = '1' else '0';
	colour_out_b <= master_b when video_enable = '1' else '0';

	-- Layer multiplexer

	multiplex: process(layers_active, colour_r, colour_g, colour_b) is
	begin
		master_r <= '0';
		master_g <= '0';
		master_b <= '0';
		for i in 0 to layers_active'high loop
			if layers_active(i) = '1' then
				master_r <= colour_r(i);
				master_g <= colour_g(i);
				master_b <= colour_b(i);
				exit;
			end if;
		end loop;
	end process;

end arch;
