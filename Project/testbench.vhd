----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    19:30:56 08/09/2018 
-- Design Name: 
-- Module Name:    testbench - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.my_types.all;
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity testbench is
end testbench;

architecture Behavioral of testbench is

signal clk	: std_logic := '0';
signal note_regs: matrix;
begin

	clk <= not clk after 20 ns;

	test_shifter: entity work.note_registers
		port map (
        clk			=> clk,
		reset		=> '0',
        row_in  	=> ("00000010", "00000010", "00000010", "00000010"),
        matrix_out	=> note_regs
		);

end Behavioral;