vlib work
vcom my_types.vhd
vcom note_registers.vhd
vcom testbench.vhd


vsim testbench

add wave -noupdate -format Logic -radix hexadecimal 	clk
add wave -noupdate -format Logic -radix hexadecimal 	note_regs

run 200ns