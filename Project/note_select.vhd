library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use work.my_types.all;

entity note_select is
	Port (
		clk			: in std_logic;
		rst			: in std_logic;

		x_coord		: in integer;
		y_coord		: in integer;
		note_matrix	: in matrix;
		render_offset	: in integer;

		the_note	: out note;
		note_x		: out integer;
		note_y		: out integer
	);
end note_select;

architecture arch of note_select is
	------------------------------------------------------------------------
	-- Signal Declarations
	------------------------------------------------------------------------

	signal x_calc	: integer;
	signal y_calc	: integer;

	signal y_with_offset : integer;

	------------------------------------------------------------------------
	-- Module Implementation
	------------------------------------------------------------------------
begin
	y_with_offset <= y_coord - render_offset;

	calculate: process(clk) is
	begin
		if (rising_edge(clk)) then
			if (note_left_limit <= x_coord and x_coord < note_right_limit) and (note_top_limit <= y_with_offset and y_with_offset < note_bottom_limit) then
				x_calc <= (x_coord - note_left_limit) / x_size_pixels;
				y_calc <= (y_with_offset - note_top_limit) / y_size_pixels;
				the_note <= note_matrix(y_calc)(x_calc);
				note_x <= (x_coord - note_left_limit) mod x_size_pixels;
				note_y <= (y_with_offset - note_top_limit) mod y_size_pixels;
			else
				the_note <= (others => '0');
			end if;
		end if;
	end process;
end arch;

