library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use work.my_types.all;

entity project is
	Port (
		clk		: in std_logic;
		astb	: in std_logic;
		dstb	: in std_logic;
		pwr		: in std_logic;
		pdata	: inout std_logic_vector(7 downto 0);
		pwait	: out std_logic;
		led		: out std_logic_vector(7 downto 0);
		fx2_io	: inout std_logic_vector(40 downto 1);
		sw		: in std_logic_vector(7 downto 0);
		btn		: in std_logic_vector(4 downto 0)
	);
end project;

architecture arch of project is
	------------------------------------------------------------------------
	-- Signal Declarations
	------------------------------------------------------------------------

	signal rst			: std_logic;

	-- score digits

	signal score_hundreds	: std_logic_vector(3 downto 0);
	signal score_tens		: std_logic_vector(3 downto 0);
	signal score_ones		: std_logic_vector(3 downto 0);

	-- Timing signals
	signal render_pulse		: std_logic;
	signal render_offset	: integer;
	signal shift_pulse		: std_logic;
	signal notes_counter	: integer;

	-- Data from VGA
	signal x_co, y_co	: integer;

	-- Data from shape modules
	signal layers_active	: std_logic_vector(layers-1 downto 0);
	signal colour_r		: std_logic_vector(layers-1 downto 0);
	signal colour_g		: std_logic_vector(layers-1 downto 0);
	signal colour_b		: std_logic_vector(layers-1 downto 0);


	-- Epp transfer signal
	signal block_ram_index_hi_sig	: std_logic_vector(7 downto 0);
	signal block_ram_index_lo_sig	: std_logic_vector(7 downto 0);
	signal block_ram_index			: integer;
	signal block_ram_data_sig		: std_logic_vector(7 downto 0);
	signal block_ram_valid_sig		: std_logic;
	signal block_ram_num_raw		: std_logic_vector(7 downto 0);
	signal block_ram_num_sig		: integer;

	-- Data to VGA pins
	signal v_sync		: std_logic;
	signal h_sync		: std_logic;
	signal colour_out_r	: std_logic;
	signal colour_out_g	: std_logic;
	signal colour_out_b	: std_logic;

	-- Data to note registers
	signal row_in		: row;

	-- Data from note registers
	signal note_matrix	: matrix;

	-- Data from note selector
	signal this_note	: note;
	signal x_in_box		: integer;
	signal y_in_box		: integer;

	signal counter		: integer := 0;
	signal counter1s	: integer := 0;
	signal basis_clock	: std_logic := '0';

	-- ADC signals
	signal sig_cs		: std_logic;
	signal sig_rd		: std_logic;
	signal sig_wr		: std_logic;
	signal raw_input	: std_logic_vector(7 downto 0);
	signal input_val	: std_logic_vector(7 downto 0);

	signal data1		: std_logic_vector(7 downto 0);
	signal data2		: std_logic_vector(7 downto 0);
	signal data3		: std_logic_vector(7 downto 0);
	signal data4		: std_logic_vector(7 downto 0);

	signal score		: integer;
	signal score_sig	: std_logic_vector(7 downto 0);

	signal rand : integer;

	-- Controller Signals
	signal button_pins	: std_logic_vector(num_buttons-1 downto 0);
	signal buttons		: std_logic_vector(num_buttons-1 downto 0);

	-- Paddle signals
	signal paddle_col	: integer;
	signal paddle_raw	: std_logic_vector(7 downto 0);

	-- Image test signals
	signal image_index_sig	: integer;
	signal image_pixel_sig	: std_logic_vector(bits-1 downto 0);

	-- Song Block RAM
	signal note_from_ram	: std_logic_vector(7 downto 0);
	signal note_address		: integer;

	-- Block ram enable signals
	signal block_ram_en_notes		: std_logic;
	signal block_ram_en_image		: std_logic;
	signal block_ram_note_select	: std_logic_vector(6 downto 0);

	-- Sound signals
	signal sound_start	: std_logic;
	signal sound_pin	: std_logic;

	signal write_en	: std_logic;

	signal write_row	: integer;
	signal write_col	: integer;
	signal write_data	: note;

	------------------------------------------------------------------------
	-- Module Implementation
	------------------------------------------------------------------------
begin
	block_ram_num_sig <= to_integer(unsigned(block_ram_num_raw));

	led <= block_ram_num_raw;

	rst <= btn(0);

	fx2_io(4 downto 1) <= (others => '0');
	fx2_io(6) <= '0';
	fx2_io(8) <= '0';
	fx2_io(10) <= '0';
	fx2_io(12) <= '0';
	fx2_io(14) <= '0';
	fx2_io(26) <= '0';
	fx2_io(36) <= '0';
	fx2_io(34 downto 28) <= (others => '0');
	fx2_io(40 downto 38) <= (others => '0');

	-- VGA output signals
	fx2_io(5) <= colour_out_r;
	fx2_io(7) <= colour_out_g;
	fx2_io(9) <= colour_out_b;
	fx2_io(11) <= h_sync;
	fx2_io(13) <= v_sync;

	-- ADC control signals
	fx2_io(23) <= sig_cs;
	fx2_io(24) <= sig_rd;
	fx2_io(25) <= sig_wr;
	raw_input <= fx2_io(22 downto 15);

	-- Sound pins
	fx2_io(27) <= sound_pin;
	fx2_io(35) <= '0';
	fx2_io(37) <= '1';

	-- Buttons
	button_pins(0) <= btn(1);

	--process (clk, rst) begin
	--	if rst = '1' then
	--		row_in <= ("00000000", "00000000", "00000000", "00000001"); -- just for test
	--		rand <= 1234;
	--	elsif rising_edge(clk) then
	--		rand <= rand * 1664525 + 1013904223;
	--		row_in <= (others => "00000000");
	--		row_in(rand / (2 ** 8) mod 4) <= std_logic_vector(to_unsigned(rand / (2 ** 27), 8));
	--	end if;
	--end process;

	-- EPP and block ram
	block_ram_index <= to_integer(unsigned(block_ram_index_hi_sig) & unsigned(block_ram_index_lo_sig));

	block_ram_en_image <= '1' when block_ram_valid_sig = '1' and block_ram_num_sig = 7 else '0';
	block_ram_en_notes <= '1' when block_ram_valid_sig = '1' and block_ram_num_sig = 8 else '0';
	block_ram_note_select <= std_logic_vector(to_unsigned(2 ** block_ram_num_sig, 7)) when block_ram_num_sig < 7 else "0000000";

	note_address <= notes_counter/2 + 1;

	note_ram: entity work.block_ram port map(
		clk			=> clk,
		sto_addr		=> block_ram_index,
		sto_data		=> block_ram_data_sig,
		sto_enable	=> block_ram_en_notes,
		ld_addr		=> note_address,
		ld_data		=> note_from_ram
	);

	timing_logic: entity work.timings port map(
		clk				=> clk,
		rst				=> rst,

		render_pulse	=> render_pulse,
		render_offset	=> render_offset,

		shift_pulse		=> shift_pulse,
		notes_counter	=> notes_counter
	);

	notes: entity work.note_registers port map(
		clk			=> clk,
		rst			=> rst,
		clk_en		=> shift_pulse,
		row_in		=> row_in,
		matrix_out	=> note_matrix,
		write_en		=> write_en,
		write_row	=> write_row,
		write_col	=> write_col,
		write_data	=> write_data
	);

	selector: entity work.note_select port map(
		clk			=> clk,
		rst			=> rst,

		x_coord		=> x_co,
		y_coord		=> y_co,
		note_matrix	=> note_matrix,
		render_offset => render_offset,

		the_note	=> this_note,

		note_x		=> x_in_box,
		note_y		=> y_in_box
	);

	note: entity work.shape_note port map(
		clk				=> clk,
		rst				=> rst,

		x_coord				=> x_in_box,
		y_coord				=> y_in_box,

		the_note			=> this_note,
		in_addr				=> block_ram_index,
		in_data				=> block_ram_data_sig,
		block_ram_sel		=> block_ram_note_select,

		layers_active		=> layers_active(3),
		colour_r			=> colour_r(3),
		colour_g			=> colour_g(3),
		colour_b			=> colour_b(3)
	);

	gl: entity work.game_logic port map(
		clk			=> clk,
		rst			=> rst,

		shift_pulse => shift_pulse,

		matrix		=> note_matrix,
		paddle		=> paddle_col,

		buttons		=> buttons,

		write_en	=> write_en,

		write_row	=> write_row,
		write_col	=> write_col,
		write_data	=> write_data,

		sound_start	=> sound_start,

		score		=> score,

		score_hundreds	=> score_hundreds,
		score_tens		=> score_tens,
		score_ones		=> score_ones
	);

	UI: entity work.shape_UI port map(
		clk				=> clk,
		rst				=> rst,

		x_coord			=> x_co,
		y_coord			=> y_co,

		layers_active		=> layers_active(2),
		colour_r			=> colour_r(2),
		colour_g			=> colour_g(2),
		colour_b			=> colour_b(2)
	);

	vga: entity work.vga_io port map(
		layers_active		=> layers_active,
		colour_r		=> colour_r,
		colour_g		=> colour_g,
		colour_b		=> colour_b,

		clk			=> clk,
		rst			=> rst,

		x_coord			=> x_co,
		y_coord			=> y_co,

		v_sync			=> v_sync,
		h_sync			=> h_sync,
		colour_out_r		=> colour_out_r,
		colour_out_g		=> colour_out_g,
		colour_out_b		=> colour_out_b
	);

	epp_module: entity work.DPIMREF port map(
		mclk				=> clk,
		pdb					=> PDATA,
		astb				=> ASTB,
		dstb				=> DSTB,
		pwr					=> PWR,
		pwait				=> PWAIT,
		rgLed				=> open,
		block_ram_index_hi	=> block_ram_index_hi_sig,
		block_ram_index_lo	=> block_ram_index_lo_sig,
		block_ram_data		=> block_ram_data_sig,
		block_ram_num		=> block_ram_num_raw,
		block_ram_valid		=> block_ram_valid_sig,
		score				=> score_sig
	);

	score_sig <= std_logic_vector(to_unsigned(score,8));

	--LED <= SW;

	shapePad: entity work.shape_paddle port map(
		clk		=> clk,
		rst		=> rst,
		x_coord		=> x_co,
		y_coord		=> y_co,
		paddle_raw	=> input_val,
		enable		=> layers_active(0),
		colour_r 	=> colour_r(0),
		colour_g 	=> colour_g(0),
		colour_b 	=> colour_b(0)
	);

	paddle_col <= to_integer(unsigned(input_val)) * cols / 256;

	shapeSnap: entity work.shape_paddle_snap port map(
		clk			=> clk,
		rst			=> rst,
		x_coord		=> x_co,
		y_coord		=> y_co,
		paddle_col	=> paddle_col,
		enable		=> layers_active(1),
		colour_r 	=> colour_r(1),
		colour_g 	=> colour_g(1),
		colour_b 	=> colour_b(1)
	);

	-- controller (paddle) reg.
	controller_io: entity work.controller port map(
		clk					=> clk,
        rst					=> rst,
		sig_cs  			=> sig_cs,
        sig_rd  			=> sig_rd,
        sig_wr  			=> sig_wr,
        raw_input			=> raw_input,
		value   			=> input_val,
		button_input		=> button_pins,
		button_debounced	=> buttons
	);

	-- sound generator
	sound_gen: entity work.sound port map(
		matrix_in		=> note_matrix,
		paddle_col		=>	paddle_col,
		start_signal	=> sound_start,
		clk				=> clk,
		rst				=> rst,
		sound_wave		=> sound_pin
	);

	-- score text displayer
	score_display: entity work.shape_score
	port map(
		-- Internal signals
		clk			=> clk,
		rst			=> rst,

		-- input signals
		score_ones	=> score_ones,
		score_tens	=> score_tens,
		score_hundreds	=> score_hundreds,

		x_coord		=> x_co,
    	y_coord		=> y_co,

		-- output signals
		enable		=> layers_active(6 downto 4),
		colour_r	=> colour_r(6 downto 4),
		colour_g	=> colour_g(6 downto 4),
		colour_b	=> colour_b(6 downto 4)
	);

	-- background image diplayer
	shapeImageTest: entity work.shape_image_test port map(
		clk			=> clk,
		rst			=> rst,
		x_coord		=> x_co,
		y_coord		=> y_co,
		in_addr		=> block_ram_index,
    	in_data		=> block_ram_data_sig,
		in_valid 	=> block_ram_en_image,
		enable		=> layers_active(7),
		colour_r 	=> colour_r(7),
		colour_g 	=> colour_g(7),
		colour_b 	=> colour_b(7)
	);

	-- notes generator/loader
	loader: entity work.song_loader port map(
		clk			=> clk,
		rst			=> rst,
		notes_counter	=> notes_counter,
		note_in		=> note_from_ram,
		row_out		=> row_in
	);

end arch;

