import sys
import re
import random

divisions = 8;

curbar = 0
curcro = 0

lines = []

def main():

	curbar = None
	curcro = None

	lines = []

	for line in sys.stdin:

		split = re.split(r" +", line.strip());

		if len(split) < 9:
			print(" ".join(split))
			continue

		if curbar == "0" and curcro == "0" and split[8] != "NT":
			print(" ".join(split))
			continue

		# if split[5] not in ["0", "1", "2", "3"]:
		# 	continue

		if len(split) > 3:
			time = re.match(r"([^+/]*)\+?([^/]*)/?(.*)", split[3]).groups()
			if time[1] != "":
				num = round(divisions * int(time[1]) / int(time[2]))
				split[3] = f"{time[0]}+{num}/{divisions}"
			elif time[2] != "":
				num = round(divisions * int(time[0]) / int(time[2]))
				split[3] = f"{num}/{divisions}"
			else:
				split[3] = time[0]

		bar = split[3]
		cro = split[3]

		if split[8] == "NT":
			split[9] = split[9][:1]
			split[10] = f"1/{divisions}"

		if bar == curbar and cro == curcro:
			lines.append(split)
			continue

		else:
			if len(lines):
				order = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16]
				lines.sort(key=lambda x: order[int(x[5])])
				print(" ".join(lines[0]))

		curbar = bar
		curcro = cro
		lines = [split]

	for pline in lines:
			print(" ".join(pline))

if __name__ == "__main__":
	main()
