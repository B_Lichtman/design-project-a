import sys
import re

perbar = 4
divisions = 8

per_note = 2

note_list = []

def finish_list():
	prev_time = 0

	for entry in note_list:
		diff = entry[0] - prev_time

		while(diff > per_note):
			print(".")
			diff -= per_note

		print(entry[1])
		prev_time = entry[0]


def main():
	for line in sys.stdin:
		line = line.strip()
		split = re.split(r" +", line.strip());

		if len(split) < 9:
			continue

		if split[8] != "NT":
			continue

		bar = int(split[1])
		add = 0
		fraction = 0

		time = re.match(r"([^+/]*)\+?([^/]*)/?(.*)", split[3]).groups()
		if time[1] != "":
			add = int(time[0])
			fraction = int(time[1]) / int(time[2])
		elif time[2] != "":
			fraction = int(time[0]) / int(time[2])
		else:
			add = int(time[0])

		final = round(perbar * divisions * (bar-1) + divisions * add + divisions * fraction)

		note = split[9]

		note_list.append((final, note.lower()))

		finish_list()



if __name__ == "__main__":
	main()
